<?php
require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

//SHEEP
echo '<h3> Sheep </h3>';
$sheep = new Animal('shaun',2,'false');
echo 'Nama hewan adalah: '.$sheep->get_name().'<br>'; // "shaun"
echo 'Jumlah kaki: '.$sheep->get_legs(). '<br>'; // 2
echo 'berdarah dingin: '.$sheep->get_cold_blooded(),'<br>'; // false

// APE
echo '<h3> Ape </h3>';
$sungokong = new Ape('kera sakti',2,'false');
echo 'Nama hewan adalah: '.$sungokong->get_name().'<br>';
echo 'Jumlah kaki: '.$sungokong->get_legs().'<br>';
echo 'berdarah dingin: '.$sungokong->get_cold_blooded(),'<br>';
echo $sungokong->get_yell().'<br>'; // "Auooo"

//FROG
echo '<h3> Frog </h3>';
$kodok = new frog('buluk',4,'false');
echo 'Nama hewan adalah: '.$kodok->get_name().'<br>';
echo 'Jumlah kaki: '.$kodok->get_legs().'<br>';
echo 'berdarah dingin: '.$kodok->get_cold_blooded(),'<br>';
echo $kodok->get_jump().'<br>';

?>